const AWS = require('aws-sdk');
const fs = require('fs');

class KwSecrets {

    static dbMain = {};
    static dbReader = {};
    static externals = {};
    static services = {};
    static firebaseConfig = {};

    static names = {
        services    : '/services',
        dbMain      : '/db/main',
        dbReader      : '/db/reader',
        externals   : '/externals',
        firebaseConfig : '/firebase/config',
    };

    /**
     * @param secrets
     * @param envType
     * @param region
     * @param names
     * @returns {Promise<void>}
     */
    static async refresh({secrets = null, envType = null, region = 'us-west-2'} = {}) {
        let kwSecrets = new KwSecrets({region});

        if (secrets === null) {
            secrets = Object.keys(KwSecrets.names);
        }

        if (!envType) {
            envType = process.env.APP_ENV || 'dev';
        }

        for (let name of secrets) {
            let secretName = envType + KwSecrets.names[name];
            try {
                let secretString = await kwSecrets.fetchSecret(secretName);
                let secretJson = JSON.parse(secretString);
                for (let key in secretJson) {
                    if (secretJson.hasOwnProperty(key)) {
                        KwSecrets[name][key] = secretJson[key];
                    }
                }
            } catch (e) {
                console.log(e);
                console.error(e);
            }
        }
    }

    /**
     *
     * @param region
     */
    constructor({region = 'us-west-2'} = {}) {
        this.client = new AWS.SecretsManager({
            region: region
        });
    }

    /**
     * @param secretName
     * @returns {Promise<*>}
     */
    async fetchSecret(secretName) {
        return new Promise((resolve, reject) => {
            try {
                this.client.getSecretValue({SecretId: secretName}, (err, data) => {
                    if (err) {
                        if (err.code === 'DecryptionFailureException')
                            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
                            // Deal with the exception here, and/or rethrow at your discretion.
                            throw err;
                        else if (err.code === 'InternalServiceErrorException')
                            // An error occurred on the server side.
                            // Deal with the exception here, and/or rethrow at your discretion.
                            throw err;
                        else if (err.code === 'InvalidParameterException')
                            // You provided an invalid value for a parameter.
                            // Deal with the exception here, and/or rethrow at your discretion.
                            throw err;
                        else if (err.code === 'InvalidRequestException')
                            // You provided a parameter value that is not valid for the current state of the resource.
                            // Deal with the exception here, and/or rethrow at your discretion.
                            throw err;
                        else if (err.code === 'ResourceNotFoundException')
                            // We can't find the resource that you asked for.
                            // Deal with the exception here, and/or rethrow at your discretion.
                            throw err;
                        else
                            throw err;
                    } else {
                        // Decrypts secret using the associated KMS CMK.
                        // Depending on whether the secret is a string or binary, one of these fields will be populated.
                        if ('SecretString' in data) {
                            resolve(data.SecretString);
                        } else {
                            resolve(new Buffer(data.SecretBinary, 'base64'));
                        }
                    }
                });
            } catch (e) {
                console.error(e);
                reject(e);
            }
        });
    }
}

module.exports = KwSecrets;