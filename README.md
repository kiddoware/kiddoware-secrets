# Kiddoware Secrets

## Installation

Install npm package:
```bash
npm i git+https://bitbucket.org/kiddoware/kiddoware-secrets.git
```

Include in your code
```js
const KwSecrets = require('kiddoware-secrets');
```

Set **APP_ENV** environment variable to 'dev' or 'prod' based on what key you want to get
```ini
APP_ENV=dev
```

## Description
There are 4 secrets available for each environment (dev and prod):

1. KwSecrets.***dbMain***
2. KwSecrets.***externals***
3. KwSecrets.***services***
4. KwSecrets.***firebaseConfig***

They are populated by calling ```KwSecrets.refresh()``` method. Each time you call this method, secrets are being refreshed from AWS Secrets Manager

Available params are:
```javascript
await KwSecrets.refresh({
    secrets : ['dbMain','externals','services','firebaseConfig'],  // array of desired secrets
    envType : 'dev',                    // env type 'dev' or 'prod'. if not specified process.env.APP_ENV will be taken
    file    : './aws-credentials.json', // file with aws credentials. if not specified local AWS env variables will be used. AWS ECS tasks are authorized
    region  : 'us-west-2'               // region (you dont really need to change that)
});
```
Method returns ```{Promise<void>}```

## Examples
To get keys
```javascript
const KwSecrets = require('kiddoware-secrets');

KwSecrets.refresh().then(() => {
    console.log(KwSecrets.dbMain);
    console.log(KwSecrets.externals);
    console.log(KwSecrets.services);
    console.log(KwSecrets.firebaseConfig);
});
```

To get production keys
```js
const KwSecrets = require('kiddoware-secrets');

KwSecrets.refresh({envType: 'prod'}).then(() => {
    console.log(KwSecrets.dbMain);
    console.log(KwSecrets.externals);
    console.log(KwSecrets.services);
    console.log(KwSecrets.firebaseConfig);
});
```

To specify different AWS accessKeyId and secretAccessKey, put them into file ***aws-credentials.json***:
```json
{
  "accessKeyId": "AKSDFWFAFFSDL3ZJA",
  "secretAccessKey": "hRJQMrt5t45143613t13ttqrtkshemYD6wAEvhRrb",
  "region": "us-west-2"
}

```
 
And pass a path to this file:
```js
KwSecrets.refresh({file: './aws-credentials.json'}).then(() => {
    console.log(KwSecrets.dbMain);
    console.log(KwSecrets.externals);
    console.log(KwSecrets.services);
    console.log(KwSecrets.firebaseConfig);
});
```

To refresh only some specific secrets:
```js
KwSecrets.refresh({secrets: ['dbMain','firebaseConfig']}).then(() => {
    console.log(KwSecrets.dbMain);
    console.log(KwSecrets.firebaseConfig);
});
```

## Use-cases

You should do ```KwSecrets.refresh()``` in case when desired service threw you an authorization error. 
That could mean that secret has been changed and you need to refresh corresponding secrets.

## Last know secrets:

### dbMain
|Key|Description|
|---|---|
|username|database username|
|password|database password|
|engine|mysql|
|host| database host|
|port|3306|


### services
|Key|Description|
|---|---|
|**Licensing API**||
|KwSecrets.services.LICENSING_ENDPOINT|https://licensing.kiddoware.com|
|KwSecrets.services.LICENSING_CLIENT_ID| api client id|
|KwSecrets.services.LICENSING_CLIENT_SECRET| api client secret|
|**Kpsb Proxy API**||
|KwSecrets.services.KPSB_PROXY_API_ENDPOINT|http://proxy.browser.kiddoware.com/|
|KwSecrets.services.KPSB_PROXY_API_KEY||
|KwSecrets.services.KPSB_PROXY_PASSWORD_KEY| |
|KwSecrets.services.KPSB_PROXY_PORT| 8111 |
|KwSecrets.services.KPSB_PROXY_HOST| proxy.browser.kiddoware.com |
|Store API||
|KwSecrets.services.STORE_API_URL|http://100.26.108.77/api/v1|


### externals

|Key|Description|
|---|---|
|**GOOGLE SUBSCRIBE**||
|KwSecrets.externals.GOOGLE_SUBSCRIBE_OAUTH_CLIENT_ID | .....apps.googleusercontent.com |
|KwSecrets.externals.GOOGLE_SUBSCRIBE_OAUTH_CLIENT_SECRET | pfFOu.... |
|KwSecrets.externals.GOOGLE_SUBSCRIBE_OAUTH_REFRESH_TOKEN | p1/DHBwG.... |
|KwSecrets.externals.GOOGLE_SUBSCRIBE_OAUTH_GRANT_TYPE | refresh_token |
|**Mail Chimp**||
|KwSecrets.externals.MAIL_CHIMP_API_KEY | 0538f88e...|
|**SendGrid**||
|KwSecrets.externals.SENDGRID_PASSWORD | pbrgfjd395hk|
|KwSecrets.externals.SENDGRID_USERNAME | pkiddoware|
|**Stripe**||
|KwSecrets.externals.STRIPE_PUBLIC_KEY | pk_test_...|
|KwSecrets.externals.STRIPE_SECRET_KEY | sk_test_...|
|**PayPal**||
|KwSecrets.externals.PAYPAL_MODE | sandbox|
|KwSecrets.externals.PAYPAL_CLIENT_ID | AYJs4RFGxtVw...|
|KwSecrets.externals.PAYPAL_CLIENT_SECRET | EB9ns7yjXPF...|